## Ejecución del SMAM
Para ejecutar el SMAM es necesario realizar los siguientes pasos. Los siguientes pasos asumen que se ha descargado el repositorio.

1. Abrir la terminal.
2. Ingresar a la ruta del SMAM:   `cd publica-suscribe/smam/`
3. Ejecutar el simulador: `python simulador.py`